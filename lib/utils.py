#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

#from parameters import *
from cons import *
import os, myxml, sys, math, subprocess
#from multiprocessing import Process, Array
from fitness import *
from math import sqrt

BIN_DIR = os.path.join(os.path.dirname(__file__), "..", "bin")
SUMO = os.path.join(BIN_DIR, "sumo")
SUMOGUI = os.path.join(BIN_DIR, "sumo-gui")
VERBOSE = False
TABLE = {}

def stats(detectorFile,start,solDict):
   """Calculates the stats of the simulation"""
   detDict = myxml.getDetectors(detectorFile,start)

   if VERBOSE:
      keys = detDict.keys()
      keys.sort();
      for k in keys:
         diff = detDict[k] - solDict[k]
         perc = float(detDict[k]) / solDict[k]
         print "%15s" % k, "%5d" % detDict[k], "%5d" % solDict[k], "%5d" % diff, "%5.2f" % perc
      print

   diffDict = {}

   f = 0.0
   for det in detDict:
      #f += abs( float(detDict[det] - solDict[det]) / float(solDict[det]) )
      #f += math.pow(detDict[det] - solDict[det], 2)
      f += math.pow( (detDict[det] - solDict[det]), 2) / float(solDict[det])
      diffDict.update(dict([(det, detDict[det] - solDict[det])]))

   f = f / float(len(detDict))

#   print LINE
   print "Fitness:",FITNESS_FORMAT % (f)
#   print LINE

   return f,diffDict

def fitness(I, configFile, templateFile, detectorFile, start, time, solDict, seeds, gui=False):
   """Calculates the fitness of the individual"""

   sumoExe = SUMO
   if gui:
      sumoExe = SUMOGUI
   saveRoutesFile(I,templateFile,"routes1.xml",time)

   extra = ""
   seed = int(seeds)

   sys.stdout.flush()
   sys.stderr.flush()
   if 0 == subprocess.call("%s -c %s -r routes1.xml --seed %d --end %d --xml-validation.net never -X never %s" % (sumoExe, configFile, seed, time, extra), shell=True, stdout=sys.stdout, stderr=sys.stderr, bufsize=-1):
      ff,diffDict = stats(detectorFile,start,solDict)
   else:
      ff = sys.float_info.max
      diffDict = {}

   return ff,diffDict

def fitness2(I1, I2, configFile1, configFile2, templateFile, detectorFile1, detectorFile2, start, time, solDict, seeds, gui=False):
   """Calculates the fitness of the individual"""

   sumoExe = SUMO
   saveRoutesFile(I1,templateFile,"routes1.xml",time)
   saveRoutesFile(I2,templateFile,"routes2.xml",time)

   extra = ""
   seed = int(seeds)

   sys.stdout.flush()
   sys.stderr.flush()

   sp1 = subprocess.Popen("%s -c %s -r routes1.xml --seed %d --end %d --xml-validation.net never -X never %s" % (sumoExe, configFile1, seed, time, extra), shell=True, stdout=sys.stdout, stderr=sys.stderr, bufsize=-1)
   sp2 = subprocess.Popen("%s -c %s -r routes2.xml --seed %d --end %d --xml-validation.net never -X never %s" % (sumoExe, configFile2, seed, time, extra), shell=True, stdout=sys.stdout, stderr=sys.stderr, bufsize=-1)

   if sp1.wait() != 0:
      f1 = sys.float_info.max
      diffDict1 = {}
   else:
      f1,diffDict1 = stats(detectorFile1,start,solDict)
   if sp2.wait() != 0:
      f2 = sys.float_info.max
      diffDict2 = {}
   else:
      f2,diffDict2 = stats(detectorFile2,start,solDict)

   return [f1,f2],[diffDict1,diffDict2]


def distanceIndividuals(I1,I2):
   """Returns the euclidean distance between individuals"""
   D = 0
   for k in I1['data'].keys():
      D += (I1['data'][k] - I2['data'][k]) ** 2
   return sqrt(D)


def encodeData(D, seed=0):
   flows = D.keys()
   flows.sort()
   string = ""
   for f in flows:
       string += '%04d' % D[f]
   return string + "." + '%09d' % seed

def duplicate(P):
   """Returns a copy of the population P"""
   if VERBOSE:
      print "Copying values..."
   result = []
   for I in P: # each individual
      result = result + [copyIndividual(I)]

   if VERBOSE:
      print "...done"
   return result

def copyIndividual(I):
   """Returns a copy of individual"""
   D = I['data']
   newFitness = I['fitness']
   newDiff = I['diff']
   newData = {}
   for d in D:
      newData.update(dict([(d,D[d])]))

   return {'fitness':newFitness, 'data':newData, 'diff':newDiff}

def copyNexts(nexts):
   newNexts = {}
   for next in nexts:
      newNexts.update(dict([(next, nexts[next])]))
   return newNexts

def checkSolutions(P):
   if VERBOSE:
      print "Checking individuals:",
   for i in range(len(P)):
      if VERBOSE:
         print str(i),
      checkIndividual(P[i])
      if VERBOSE:
         print "OK",
   print

def checkIndividual(I):

   def check(values):
      for value in values:
         if value < MIN_PROB or value > MAX_PROB:
            raise Exception("Value is out of range: "+str(value)+"\n"+str(next))

   D = I['data']
   for source in D:
      for next in D[source]:
         check(D[source].values())

def printIndividualVector(I,sep=False):
   od = ""
   cd = ""
   ss = ""
   if sep:
      od = "["
      cd = "]"
      ss = " "

   print LINE
   D = I['data']
   sources = D.keys()
   sources.sort()
   string = ""
   for source in sources:
      destinations = D[source].keys()
      destinations.sort()
      for destination in destinations:
         string += od
         nexts = D[source][destination].keys()
         nexts.sort()
         for next in nexts:
            string += '%03d' % D[source][destination][next]
         string += cd
      string += ss
   print string + " " + FITNESS_FORMAT % I['fitness']
   print LINE

def printStatusVector(P,sep=False):
   print DBLLINE
   for i in range(len(P)): # each individual
      print "Individual:",i
      printIndividualVector(P[i],sep)
   print DBLLINE

def printPopulation(P):
   print DBLLINE
   for i in range(len(P)): # each individual
      print "Individual:",i
      printIndividual(P[i])
   print DBLLINE

def printIndividual(I):

   print LINE
   print "Fitness:",I['fitness']
   D = I['data']
   sources = D.keys()
   sources.sort()
   for source in sources:
      print " Source:",source
      destinations = D[source].keys()
      destinations.sort()
      for destination in destinations:
         print "   D:",destination," T:",D[source][destination]
   print LINE

def printTimes(I):

   print LINE
   print "%9.3f" % I['fitness'],
   D = I['data']
   sources = D.keys()
   sources.sort()
   for source in sources:
      destinations = D[source].keys()
      destinations.sort()
      for destination in destinations:
         print "%3.0f" % D[source][destination],
   print
   print LINE

def printRoutes(R):
   print DBLLINE
   sources = R.keys()
   sources.sort()
   for source in sources:
      print " Source:",source
      for routeType in R[source]:
         print "   Type:",routeType
         destinations = R[source][routeType].keys()
         destinations.sort()
         for destination in destinations:
            print "      D:",destination
            for route in R[source][routeType][destination]:
               print "         R:",route[0],'->',route[-1]
      print
   print DBLLINE

def vectorSize(populationfile):
   I = myxml.getPopulation(populationfile)
   D = I['data']
   ac = 0
   sources = D.keys()
   for source in sources:
      destinations = D[source].keys()
      for destination in destinations:
         ac += len(D[source][destination].keys())
   print ac

def problemSize():
    I = myxml.getSensorConfig()
    data = I['data']

    ac = 1
    for sensor in data:
        print sensor
        for routeType in data[sensor]:
            for destination in data[sensor][routeType]:
                print destination, len(data[sensor][routeType][destination])
                ac *= len(data[sensor][routeType][destination])
    print '%1.3g' % (ac)


def summary(outputFileName,filename=SUMMARY_FILENAME):

   SL = myxml.getSummaryList(filename)
   summaryFile = open(outputFileName, 'w')
   summaryFile.write('"time"'+SEP+'"loaded"'+SEP+'"emitted"'+SEP+'"running"'+SEP+'"waiting"'+SEP+'"ended"'+SEP+'"meanWaitingTime"'+SEP+'"meanTravelTime"'+SEP+'"duration"\n')
   for e in SL:
      time = e['time']
      loaded = e['loaded']
      emitted = e['emitted']
      running = e['running']
      waiting = e['waiting']
      ended = e['ended']
      meanWaitingTime = e['meanWaitingTime']
      meanTravelTime = e['meanTravelTime']
      duration = e['duration']
      summaryFile.write(str(time)+SEP+str(loaded)+SEP+str(emitted)+SEP+str(running)+SEP+str(waiting)+SEP+str(ended)+SEP+str(meanWaitingTime)+SEP+str(meanTravelTime)+SEP+str(duration)+"\n")
   summaryFile.close()
   print outputFileName,"written."

def emissions(filename=EMISSION_FILENAME, emissionFileName=None):
   """Returns total emissions"""
   vehicleDict = myxml.getEmissions(filename)

   co2 = 0
   co = 0
   hc = 0
   nox = 0
   pmx = 0
   fuel = 0
   noise = 0

   for vh in vehicleDict.keys():
      rec = vehicleDict[vh]
      co2 += rec['co2']
      co += rec['co']
      hc += rec['hc']
      nox += rec['nox']
      pmx += rec['pmx']
      fuel += rec['fuel']
      noise += rec['noise']

      if emissionFileName!=None:
         statsFile.write('"'+vh+'"'+SEP+str(lenght)+'\n')

   n = len(vehicleDict)
   print "vehicles:",n
   print "Co:",co
   print "Co2:",co2
   print "Hc:",hc
   print "Pm:",pmx
   print "No:",nox
   print "Fuel:",fuel
   print "Noise:",noise

   return [n,co,co2,hc,pmx,nox,fuel,noise]

def distance(filename=NETSTATE_FILENAME, distanceFileName=None):
   """Returns average distance, total distance and standard deviation"""
   vehicleDict = myxml.getVehicleDict(filename)
   length = 0.0
   routeLengthList = []
   routeLengthList2 = []

   if distanceFileName!=None:
      statsFile = open(distanceFileName, 'w')
      statsFile.write('"vehicle"'+SEP+'"lenght"\n')

   n = 0
   for vh in vehicleDict.keys():
      rec = vehicleDict[vh]
      lenght = rec['dist']
      routeLengthList.append(lenght)
      routeLengthList2.append(lenght**2)
      n = n + 1
      if distanceFileName!=None:
         statsFile.write('"'+vh+'"'+SEP+str(lenght)+'\n')

   if distanceFileName!=None:
      statsFile.close()
      print distanceFileName,"written."

   sumRouteLength = sum(routeLengthList)
   sumRouteLength2 = sum(routeLengthList2)

   print LINE
   print "Vehicles:",n
   avgRouteLength = sumRouteLength / n
   devRouteLength = math.sqrt((sumRouteLength2 / n) - (avgRouteLength * avgRouteLength))
   #medRouteLength = median(routeLengthList)
   fmtTot = '%12.3f'
   fmtAvg = '%9.3f'
   fmtDev = '%9.3f'
   fmtMed = '%9.3f'
   #print "Total:",fmtTot % (sumRouteLength),"Average:",fmtAvg % (avgRouteLength),"Standard Dev:",fmtDev % (devRouteLength),"Median:",fmtMed % (medRouteLength)

   print "Total:",fmtTot % (sumRouteLength),"Average:",fmtAvg % (avgRouteLength),"Standard Dev:",fmtDev % (devRouteLength)

   print LINE
   return [avgRouteLength,sumRouteLength,devRouteLength]

def maxElements(v, size):
   if size > len(v):
      raise Exception("size has to be equal or less than the lenght of v")
   MAX = []
   for i in range(size):
      maxdis = 0
      maxpos = 0
      for i in range(len(v)):
         if i not in MAX:
            if v[i] >= maxdis:
               maxdis = v[i]
               maxpos = i
      MAX.append(maxpos)
   return MAX

def point_in_poly(x,y,poly):
   """Returns True if the point (x,y) is inside the poly"""

   n = len(poly)
   inside = False

   p1x,p1y = poly[0]
   for i in range(n+1):
      p2x,p2y = poly[i % n]
      if y > min(p1y,p2y):
         if y <= max(p1y,p2y):
            if x <= max(p1x,p2x):
               if p1y != p2y:
                  xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
               if p1x == p2x or x <= xints:
                  inside = not inside
      p1x,p1y = p2x,p2y

   return inside


def getFlowIds(templateFile):
   IDS = []
   tf = open(templateFile, 'r')
   for line in tf:
      pos = line.find("<routeDistribution id=")
      if pos != -1:
         IDS.append(line[pos+23:pos+23+3])
   tf.close()
   return IDS

def saveRoutesFile(I,templateFile,destinationFile,time):
   """Saves the routes file"""

   df = open(destinationFile, 'w')
   tf = open(templateFile, 'r')
   for line in tf:
      df.write(line),
   tf.close()

   #print I
   keys = I['data'].keys()
   keys.sort()

   for k in keys:
      df.write('    <flow id="'+str(k)+'" type="sedan" begin="0.0" route="'+k+'" number="'+str(I['data'][k])+'" end="'+str(time)+'" departSpeed="max" departLane="best" />\n')
   df.write('</routes>')
   df.close()


def getIndividual(individualFile):
   """Loads individual"""

   I = []
   indF = open(individualFile, 'r')
   for line in indF:
      I.append(line)
   indf.close()
   return I


