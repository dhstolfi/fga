#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

# fitness
# NUM_VEHICLES = 2023 # 1200 # 800 # 4100
# Depart Delay
W1 = 0
# Wait Steps
W2 = 0
# Duration
W3 = 1
# Co
W4 = 0  #165
# Co2
W5 = 0
# Hc
W6 = 0
# Pm
W7 = 0
# No
W8 = 0
# Fuel
W9 = 0
# Vaporized
WV = 10000000000

SIMULATION_LENGTH = 4000000 #2400000
MAX_PROB = 100
