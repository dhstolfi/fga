#!/usr/bin/env python
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
from cons import *

class PopulationHandler(ContentHandler):

   def __init__(self, which):
      self.which = which
      self.current = False
      self.population = False
      self.individual = False
      self.input = False

      self.inputDict = dict()
      self.parameterDict = dict()

   def startElement(self, name, attrs):
      if name == "population":
         self.population = True
      if name == "individual":
         self.individual = True
      if name == "input":
         self.input = True

      if self.population and self.individual and not self.input:
         self.current = self.which == int(attrs.get("id"))
         self.fitness = float(attrs.get("fitness"))

      elif self.input:
         self.inputDict.update(dict([(attrs.get("id").encode('ascii'),int(attrs.get("value")))]))

   def endElement(self,name):

      if self.input:
         if name == "input":
            self.input = False
         else:
            raise Exception("Error (/input): " + name)

      elif self.individual:
         if name == "individual":
            if self.current:
               self.parameterDict = {'fitness':self.fitness, 'data':self.inputDict}
            self.inputDict = dict()
            self.individual = False
         else:
            raise Exception("Error (/individual): " + name)

      elif self.population:
         if name == "population":
            self.population = False
         else:
            raise Exception("Error (/population): " + name)

class RoutesHandler(ContentHandler):

   def __init__(self):
      self.routes = False
      self.source = False
      self.tl = False
      self.des = False
      self.destination = False
      self.route = False
      self.lane = False

      self.sensorId = ""
      self.destinationId = ""
      self.laneList = []
      self.routeList = []
      self.tlDict = dict()
      self.desDict = dict()
      self.sourceDict = dict()

   def startElement(self, name, attrs):
      if name == "routes":
         self.routes = True
      if name == "source":
         self.source = True
      if name == "tl":
         self.tl = True
      if name == "des":
         self.des = True
      if name == "destination":
         self.destination = True
      if name == "route":
         self.route = True
      if name == "lane":
         self.lane = True

      if self.source and not self.tl and not self.des:
         self.sensorId = attrs.get("id").encode('ascii')

      elif (self.tl or self.des) and self.destination and not self.route:
         self.destinationId = attrs.get("id").encode('ascii')

      elif self.lane:
         self.laneList.append(attrs.get("id").encode('ascii'))

   def endElement(self,name):
      if self.lane:
         if name == "lane":
            self.lane = False
         else:
            raise Exception("Error (/lane): " + name)

      elif self.route:
         if name == "route":
            self.routeList.append(self.laneList)
            self.laneList = []
            self.route = False
         else:
            raise Exception("Error (/route): " + name)

      elif self.destination:
         if name == "destination":
            if self.tl:
               self.tlDict.update(dict([(self.destinationId,self.routeList)]))
            elif self.des:
               self.desDict.update(dict([(self.destinationId,self.routeList)]))
            else:
               raise Exception("Error (/destination): " + name)
            self.routeList = []
            self.destinationId = ""
            self.destination = False
         else:
            raise Exception("Error (/destination): " + name)

      elif self.tl:
         if name == "tl":
            self.tl = False
         else:
            raise Exception("Error (/tl): " + name)

      elif self.des:
         if name == "des":
            self.des = False
         else:
            raise Exception("Error (/des): " + name)

      elif self.source:
         if name == "source":
            self.sourceDict.update(dict([(self.sensorId, dict([('TL', self.tlDict), ('DES', self.desDict)]))]))
            self.tlDict = dict()
            self.desDict = dict()
            self.source = False
         else:
            raise Exception("Error (/source): " + name)

      elif self.routes:
         if name == "routes":
            self.routes = False
         else:
            raise Exception("Error (/routes): " + name)

      else:
         raise Exception("Error (/):" + name)


class RoutesZonesHandler(ContentHandler):

   def __init__(self):
      self.routes = False
      self.source = False
      self.destination = False
      self.route = False
      self.lane = False

      self.sensorId = ""
      self.destinationId = ""
      self.laneList = []
      self.routeList = []
      self.desDict = dict()
      self.sourceDict = dict()

   def startElement(self, name, attrs):
      if name == "routes":
         self.routes = True
      if name == "source":
         self.source = True
      if name == "destination":
         self.destination = True
      if name == "route":
         self.route = True
      if name == "lane":
         self.lane = True

      if self.source and not self.destination:
         self.sensorId = attrs.get("id").encode('ascii')

      elif self.destination and not self.route:
         self.destinationId = attrs.get("id").encode('ascii')

      elif self.lane:
         self.laneList.append(attrs.get("id").encode('ascii'))

   def endElement(self,name):

      if self.lane:
         if name == "lane":
            self.lane = False
         else:
            raise Exception("Error (/lane): " + name)

      elif self.route:
         if name == "route":
            self.routeList.append(self.laneList)
            self.laneList = []
            self.route = False
         else:
            raise Exception("Error (/route): " + name)

      elif self.destination:
         if name == "destination":
            self.desDict.update(dict([(self.destinationId,self.routeList)]))
            self.routeList = []
            self.destinationId = ""
            self.destination = False
         else:
            raise Exception("Error (/destination): " + name)

      elif self.source:
         if name == "source":
            self.sourceDict.update(dict([(self.sensorId, self.desDict)]))
            self.desDict = dict()
            self.source = False
         else:
            raise Exception("Error (/source): " + name)

      elif self.routes:
         if name == "routes":
            self.routes = False
         else:
            raise Exception("Error (/routes): " + name)

      else:
         raise Exception("Error (/):" + name)

class TripHandler(ContentHandler):

   def __init__(self):
      self.tripinfos = False
      self.tripinfo = False
      self.emissions = False

      self.vaporized = ""
      self.duration = 0.0
      self.departDelay = 0.0
      self.waitSteps = 0.0
      self.routeLength = 0.0
      self.arrival = 0.0
      self.co = 0.0
      self.co2 = 0.0
      self.hc = 0.0
      self.pm = 0.0
      self.no = 0.0
      self.fuel = 0.0
      self._id = ""
      self.vType = ""
      self.depart = 0.0
      self.tripList = []

   def startElement(self, name, attrs):
      if name == "tripinfos":
         self.tripinfos = True
      if name == "tripinfo":
         self.tripinfo = True
      if name == "emissions":
         self.emissions = True

      if self.tripinfos and self.tripinfo and self.emissions:
         if name == "emissions":
            self.co = float(attrs.get("CO_abs"))
            self.co2 = float(attrs.get("CO2_abs"))
            self.hc = float(attrs.get("HC_abs"))
            self.pm = float(attrs.get("PMx_abs"))
            self.no = float(attrs.get("NOx_abs"))
            self.fuel = float(attrs.get("fuel_abs"))
         else:
            raise Exception("Error (emissions): " + name)

      elif self.tripinfos and self.tripinfo and not self.emissions:
         if name == "tripinfo":
            self._id = attrs.get("id").encode('ascii')
            self.vType = attrs.get("vType").encode('ascii')
            self.depart = float(attrs.get("depart"))
            self.duration = float(attrs.get("duration"))
            self.departDelay = float(attrs.get("departDelay"))
            self.waitSteps = float(attrs.get("waitSteps"))
            self.routeLength = float(attrs.get("routeLength"))
            self.vaporized = attrs.get("vaporized")
            self.arrival = float(attrs.get("arrival"))
         else:
            raise Exception("Error (tripinfo): " + name)
      elif self.tripinfos and not self.tripinfo and not self.emissions:
         if name != "tripinfos":
            raise Exception("Error (tripinfos): " + name)
      else:
         raise Exception("Error (): " + name)

   def endElement(self,name):
      if self.tripinfos and self.tripinfo and self.emissions:
         if name == "emissions":
            self.emissions = False
         else:
            raise Exception("Error (/emissions): " + name)
      elif self.tripinfos and self.tripinfo and not self.emissions:
         if name == "tripinfo":
            self.tripList = self.tripList + [{'duration':self.duration, \
               'departDelay': self.departDelay, 'waitSteps': self.waitSteps, \
               'routeLength':self.routeLength, 'vaporized':self.vaporized, \
               'arrival':self.arrival, 'co':self.co, 'co2':self.co2, \
               'hc':self.hc, 'pm':self.pm, 'no':self.no, 'fuel':self.fuel, \
               'id':self._id, 'vtype':self.vType, 'depart':self.depart}]
            self.co = 0.0
            self.co2 = 0.0
            self.hc = 0.0
            self.pm = 0.0
            self.no = 0.0
            self.fuel = 0.0
            self.tripinfo = False
         else:
            raise Exception("Error (/tripinfo): " + name)
      elif self.tripinfos and not self.tripinfo and not self.emissions:
         if name == "tripinfos":
            self.tripinfos = False
         else:
            raise Exception("Error (/tripinfos): " + name)
      else:
         raise Exception("Error (/):" + name)

class SummaryHandler(ContentHandler):

   def __init__(self):
      self.summary = False
      self.step = False

      self.time = 0.0
      self.loaded = 0.0
      self.emitted = 0.0
      self.running = 0.0
      self.waiting = 0.0
      self.ended = 0.0
      self.waitingTime = 0.0
      self.travelTime = 0.0
      self.duration = 0.0
      self.summaryList = []

   def startElement(self, name, attrs):
      if name == "summary":
         self.summary = True
      if name == "step":
         self.step = True

      if self.summary and self.step:
         if name == "step":
               self.time = float(attrs.get("time"))
               self.loaded = float(attrs.get("loaded"))
               #self.emitted = float(attrs.get("emitted"))
               self.emitted = float(attrs.get("inserted"))
               self.running = float(attrs.get("running"))
               self.waiting = float(attrs.get("waiting"))
               self.ended = float(attrs.get("ended"))
               self.waitingTime = float(attrs.get("meanWaitingTime"))
               self.travelTime = float(attrs.get("meanTravelTime"))
               self.duration = float(attrs.get("duration"))
         else:
               raise Exception("Error (step): " + name)
      elif self.summary and not self.step:
         if name != "summary":
               raise Exception("Error (summary): " + name)
      else:
         raise Exception("Error (): " + name)

   def endElement(self,name):
      if self.summary and self.step:
         if name == "step":
               self.summaryList = self.summaryList + [{'time':self.time, \
                  'loaded': self.loaded, 'emitted': self.emitted, \
                  'running':self.running, 'waiting':self.waiting, \
                  'ended':self.ended, 'meanWaitingTime':self.waitingTime, \
                  'meanTravelTime':self.travelTime, 'duration':self.duration}]
               self.step = False
         else:
               raise Exception("Error (/step): " + name)
      elif self.summary and not self.step:
         if name == "summary":
               self.summary = False
         else:
               raise Exception("Error (/summary): " + name)
      else:
         raise Exception("Error (/):" + name)

class GeoHandler(ContentHandler):

   def __init__(self):
      self.geo = False
      self.step = False
      self.vehicles = False
      self.vehicle = False
      self.pois = False
      self.poi = False
      self.polygons = False
      self.polygon = False

      self.stepValue = 0
      self.geoDict = {}

      self.vehicleList = []
      self.vehicleDict = {}
      self.poiList = []
      self.poiDict = {}
      self.polygonList = []
      self.polygonDict = {}

   def startElement(self, name, attrs):
      if name == "geo":
         self.geo = True
      elif name == "step":
         self.step = True
      elif name == "vehicles":
         self.vehicles = True
      elif name == "vehicle":
         self.vehicle = True
      elif name == "pois":
         self.pois = True
      elif name == "poi":
         self.poi = True
      elif name == "polygons":
         self.polygons = True
      elif name == "polygon":
         self.polygon = True
      else:
         raise Exception("Error openning:" + name)

      if self.geo and self.step and \
         not self.vehicles and not self.vehicle and \
         not self.pois and not self.poi and \
         not self.polygons and not self.polygon:

         self.stepValue = int(attrs.get("value"))

      if self.geo and self.step and \
         self.vehicles and self.vehicle and \
         not self.pois and not self.poi and \
         not self.polygons and not self.polygon:

         self.vehicleDict = \
                  {'id':attrs.get("id").encode('ascii'), \
                  'x':float(attrs.get("x")), \
                  'y':float(attrs.get("y")), \
                  'speed':float(attrs.get("speed")), \
                  'type':attrs.get("type").encode('ascii'), \
                  'angle':float(attrs.get("angle")), \
                  'accel':float(attrs.get("accel")), \
                  'decel':float(attrs.get("decel")), \
                  'length':float(attrs.get("length")), \
                  'width':float(attrs.get("width")), \
                  'maxSpeed':float(attrs.get("maxSpeed"))}

      elif self.geo and self.step and \
         not self.vehicles and not self.vehicle and \
         self.pois and self.poi and \
         not self.polygons and not self.polygon:

         self.poiDict = \
                  {'id':attrs.get("id").encode('ascii'), \
                  'x':float(attrs.get("x")), \
                  'y':float(attrs.get("y")), \
                  'type':attrs.get("type").encode('ascii')}

      elif self.geo and self.step and \
         not self.vehicles and not self.vehicle and \
         not self.pois and not self.poi and \
         self.polygons and self.polygon:

         self.polygonDict = \
                  {'id':attrs.get("id").encode('ascii'), \
                  'x':float(attrs.get("x")), \
                  'y':float(attrs.get("y")), \
                  'type':attrs.get("type").encode('ascii')}

   def endElement(self,name):
      if name == "geo":
         self.geo = False
         self.geoDict = {'step':self.stepValue, \
                           'vehicles':self.vehicleList, \
                           'pois':self.poiList, \
                           'polygons':self.polygonList }
      elif name == "step":
         self.step = False
      elif name == "vehicles":
         self.vehicles = False
      elif name == "vehicle":
         self.vehicle = False
         self.vehicleList.append(self.vehicleDict)
      elif name == "pois":
         self.pois = False
      elif name == "poi":
         self.poi = False
         self.poiList.append(self.poiDict)
      elif name == "polygons":
         self.polygons = False
      elif name == "polygon":
         self.polygon = False
         self.polygonList.append(self.polygonDict)
      else:
         raise Exception("Error closing:" + name)

class NetStateHandler(ContentHandler):

   def __init__(self):
      self.netstate = False
      self.timestep = False
      self.edge = False
      self.lane = False
      self.vehicle = False

      self.currentEdge = ""
      self.vehicleDict = {}

   def startElement(self, name, attrs):
      if name == "netstate":
         self.netstate = True
      elif name == "timestep":
         self.timestep = True
      elif name == "edge":
         self.edge = True
         self.currentEdge = attrs.get("id")
      elif name == "lane":
         self.lane = True
      elif name == "vehicle":
         self.vehicle = True
      else:
         raise Exception("Error openning:" + name)

      if self.netstate and self.timestep and \
         self.edge and self.lane and self.vehicle:

         vh = attrs.get("id")
         pos = float(attrs.get("pos"))

         if vh in self.vehicleDict:
            rec = self.vehicleDict[vh]
            if rec['edge']==self.currentEdge:
               rec['pos'] = pos
            else:
               rec['edge'] = self.currentEdge
               rec['dist'] = rec['dist'] + rec['pos']
               #print rec['pos'],rec['dist']
               rec['pos'] = pos
         else:
            self.vehicleDict.update(dict([(vh, {'edge':self.currentEdge, 'dist':0.0, 'pos':pos})]))

   def endElement(self,name):
      if name == "netstate":
         self.netstate = False
         for vh in self.vehicleDict.keys():
            rec = self.vehicleDict[vh]
            rec['dist'] = rec['dist'] + rec['pos']
            rec['pos'] = 0
      elif name == "timestep":
         self.timestep = False
      elif name == "edge":
         self.edge = False
         self.currentEdge = ""
      elif name == "lane":
         self.lane = False
      elif name == "vehicle":
         self.vehicle = False
      else:
         raise Exception("Error closing:" + name)

class EmissionHandler(ContentHandler):

   def __init__(self):
      self.emission = False
      self.timestep = False
      self.vehicle = False

      self.vehicleDict = {}

   def startElement(self, name, attrs):
      if name == "emission-export":
         self.emission = True
      elif name == "timestep":
         self.timestep = True
      elif name == "vehicle":
         self.vehicle = True
      else:
         raise Exception("Error openning:" + name)

      if self.emission and self.timestep and self.vehicle:

         vh = attrs.get("id")
         co2 = float(attrs.get("CO2"))
         co = float(attrs.get("CO"))
         hc = float(attrs.get("HC"))
         nox = float(attrs.get("NOx"))
         pmx = float(attrs.get("PMx"))
         fuel = float(attrs.get("fuel"))
         noise = float(attrs.get("noise"))

         if vh in self.vehicleDict:
            rec = self.vehicleDict[vh]
            co2 += rec['co2']
            co += rec['co']
            hc += rec['hc']
            nox += rec['nox']
            pmx += rec['pmx']
            fuel += rec['fuel']
            noise += rec['noise']

         self.vehicleDict.update(dict([(vh, {'co2':co2, 'co':co, 'hc':hc, 'nox':nox, 'pmx':pmx, 'fuel':fuel, 'noise':noise})]))

   def endElement(self,name):
      if name == "emission-export":
         self.netstate = False
      elif name == "timestep":
         self.timestep = False
      elif name == "vehicle":
         self.vehicle = False
      else:
         raise Exception("Error closing:" + name)

class NetHandler(ContentHandler):

   def __init__(self):
      self.net = False
      self.junction = False
      self.request = False

      self.laneList = []
      self.laneDict = None

   def startElement(self, name, attrs):
      if name == "net":
         self.net = True
      if name == "junction":
         self.junction = True
      if name == "request":
         self.request = True

      if self.net and self.junction and not self.request:
         id = attrs.get("id").encode('ascii')
         lanes = attrs.get("incLanes").encode('ascii')
         if lanes != "":
            if attrs.get("type").encode('ascii') != "internal":
               self.laneDict = {'lanes':lanes.split(" "), \
                                'x':float(attrs.get("x")), 'y':float(attrs.get("y")),
                                'id':id}

   def endElement(self,name):
      if self.net and self.junction and not self.request:
         if name == "junction":
            if self.laneDict != None:
               self.laneList.append(self.laneDict)
            self.junction = False
            self.laneDict = None
         else:
            raise Exception("Error (/junction):" + name)

      elif self.net and self.junction and self.request:
         if name == "request":
            self.request = False
         else:
            raise Exception("Error (/request):" + name)

class PolyHandler(ContentHandler):

   def __init__(self):
      self.shapes = False
      self.poly = False

      self.polyList = []
      self.shapeDict = None

   def startElement(self, name, attrs):
      if name == "shapes":
         self.shapes = True
      if name == "poly":
         self.poly = True

      if self.shapes and self.poly:
         if attrs.get("type").encode('ascii') == "boundary.administrative":
            shape = attrs.get("shape").encode('ascii').split(" ")
            shapeList = []
            i = 1
            for s in shape:
               if i != len(shape):
                  [x,y] = s.split(",")
                  shapeList.append((float(x), float(y)))
               i += 1
            self.shapeDict = {'id':attrs.get("id").encode('ascii'),
                              'points': shapeList}

   def endElement(self,name):
      if self.shapes and self.poly:
         if name == "poly":
            if self.shapeDict != None:
               self.polyList.append(self.shapeDict)
            self.poly = False
            self.shapeDict = None
         else:
            raise Exception("Error (/poly):" + name)

class ZoneHandler(ContentHandler):

   def __init__(self):
      self.zones = False
      self.zone = False
      self.sensors = False
      self.sensor = False
      self.streets = False
      self.street = False

##      self.polyList = []
##      self.shapeDict = None
      self.zoneId = ""
      self.sensorList = []
      self.streetList = []
      self.zoneDict = {}

   def startElement(self, name, attrs):
      if name == "zones":
         self.zones = True
      if name == "zone":
         self.zone = True
      if name == "sensors":
         self.sensors = True
      if name == "sensor":
         self.sensor = True
      if name == "streets":
         self.streets = True
      if name == "street":
         self.street = True

      if self.zones and self.zone and not self.sensors and not self.streets:
         self.zoneId = attrs.get("id").encode('ascii')

      elif self.zones and self.zone and self.sensors and self.sensor:
         self.sensorList.append(attrs.get("id").encode('ascii'))

      elif self.zones and self.zone and self.streets and self.street:
         self.streetList.append(attrs.get("id").encode('ascii'))

   def endElement(self,name):
      if self.zones and not self.zone and not self.sensors and not self.streets:
         if name == "zones":
            self.zones = False
         else:
            raise Exception("Error (/zones):" + name)

      elif self.zones and self.zone and not self.sensors and not self.streets:
         if name == "zone":
            self.zoneDict.update(dict([(self.zoneId, {'sensors':self.sensorList, 'streets':self.streetList})]))
            self.sensorList = []
            self.streetList = []
            self.zone = False
         else:
            raise Exception("Error (/zone):" + name)

      elif self.zones and self.zone and self.sensors and not self.sensor and not self.streets:
         if name == "sensors":
            self.sensors = False
         else:
            raise Exception("Error (/sensors):" + name)

      elif self.zones and self.zone and self.sensors and self.sensor and not self.streets:
         if name == "sensor":
            self.sensor = False
         else:
            raise Exception("Error (/sensor):" + name)

      elif self.zones and self.zone and not self.sensors and self.streets and not self.street:
         if name == "streets":
            self.streets = False
         else:
            raise Exception("Error (/streets):" + name)

      elif self.zones and self.zone and not self.sensors and self.streets and self.street:
         if name == "street":
            self.street = False
         else:
            raise Exception("Error (/street):" + name)

      else:
         raise Exception("Error: (/):" + name)

class SensorHandler(ContentHandler):

   def __init__(self):
      self.sensors = False
      self.sensor = False
      self.streets = False
      self.street = False

      self.sensorId = ""
      self.streetList = []
      self.sensorDict = {}

   def startElement(self, name, attrs):
      if name == "sensors":
         self.sensors = True
      if name == "sensor":
         self.sensor = True
      if name == "streets":
         self.streets = True
      if name == "street":
         self.street = True

      if self.sensors and self.sensor and not self.streets:
         self.sensorId = attrs.get("id").encode('ascii')

      elif self.sensors and self.sensor and self.streets and self.street:
         self.streetList.append(attrs.get("id").encode('ascii'))

   def endElement(self,name):
      if self.sensors and not self.sensor and not self.streets:
         if name == "sensors":
            self.sensors = False
         else:
            raise Exception("Error (/sensors):" + name)

      elif self.sensors and self.sensor and not self.streets:
         if name == "sensor":
            self.sensorDict.update(dict([(self.sensorId, self.streetList)]))
            self.streetList = []
            self.sensor = False
         else:
            raise Exception("Error (/sensor):" + name)

      elif self.sensors and self.sensor and self.streets and not self.street:
         if name == "streets":
            self.streets = False
         else:
            raise Exception("Error (/streets):" + name)

      elif self.sensors and self.sensor and self.streets and self.street:
         if name == "street":
            self.street = False
         else:
            raise Exception("Error (/street):" + name)

      else:
         raise Exception("Error: (/):" + name)

class DetectorHandler(ContentHandler):

   def __init__(self,start):
      self.detector = False
      self.interval = False
      self.start = start

      self.detectorDict = {}

   def startElement(self, name, attrs):
      if name == "detector":
         self.detector = True
      if name == "interval":
         self.interval = True

      if self.detector and self.interval:
         detId = attrs.get("id").encode('ascii')[:-2]
         numVh = int(attrs.get("nVehContrib").encode('ascii'))
         begin = float(attrs.get("begin").encode('ascii'))
         if begin>=self.start:
            if detId in self.detectorDict:
               numVh += self.detectorDict[detId]
            self.detectorDict.update(dict([(detId, numVh)]))

   def endElement(self,name):
      if self.detector and not self.interval:
         if name == "detector":
            self.detector = False
         else:
            raise Exception("Error (/detector):" + name)

      elif self.detector and self.interval:
         if name == "interval":
            self.interval = False
         else:
            raise Exception("Error (/interval):" + name)

      else:
         raise Exception("Error: (/):" + name)

class SolutionHandler(ContentHandler):

   def __init__(self):
      self.solution = False
      self.detector = False

      self.solutionDict = {}

   def startElement(self, name, attrs):
      if name == "solution":
         self.solution = True
      if name == "detector":
         self.detector = True

      if self.solution and self.detector:
         solId = attrs.get("id").encode('ascii')
         value = int(attrs.get("value").encode('ascii'))
         self.solutionDict.update(dict([(solId, value)]))

   def endElement(self,name):
      if self.solution and not self.detector:
         if name == "solution":
            self.solution = False
         else:
            raise Exception("Error (/solution):" + name)

      elif self.solution and self.detector:
         if name == "detector":
            self.detector = False
         else:
            raise Exception("Error (/detector):" + name)

      else:
         raise Exception("Error: (/):" + name)

class FlowHandler(ContentHandler):

   def __init__(self):
      self.solution = False
      self.detector = False

      self.solutionDict = {}

   def startElement(self, name, attrs):
      if name == "solution":
         self.solution = True
      if name == "detector":
         self.detector = True

      if self.solution and self.detector:
         solId = attrs.get("id").encode('ascii')
         value = int(attrs.get("value").encode('ascii'))
         self.solutionDict.update(dict([(solId, value)]))

   def endElement(self,name):
      if self.solution and not self.detector:
         if name == "solution":
            self.solution = False
         else:
            raise Exception("Error (/solution):" + name)

      elif self.solution and self.detector:
         if name == "detector":
            self.detector = False
         else:
            raise Exception("Error (/detector):" + name)

      else:
         raise Exception("Error: (/):" + name)

class RelationHandler(ContentHandler):

   def __init__(self):
      self.relations = False
      self.flow = False
      self.detector = False

      self.flowId = ""
      self.detectorDict = {}
      self.relationDict = {}

   def startElement(self, name, attrs):
      if name == "relations":
         self.relations = True

      if name == "flow":
         self.flow = True

      if name == "detector":
         self.detector = True

      if self.relations and self.flow and not self.detector:
         self.flowId = attrs.get("id").encode('ascii')

      if self.relations and self.flow and self.detector:
         detectorId = attrs.get("id").encode('ascii')
         value = int(attrs.get("value").encode('ascii'))
         self.detectorDict.update(dict([(detectorId, value)]))


   def endElement(self,name):
      if self.relations and self.flow and self.detector:
         if name == "detector":
            self.detector = False
         else:
            raise Exception("Error (/detector):" + name)

      elif self.relations and self.flow and not self.detector:
         if name == "flow":
            self.flow = False
            self.relationDict.update(dict([(self.flowId, self.detectorDict)]))
            self.detectorDict = {}
         else:
            raise Exception("Error (/flow):" + name)

      elif self.relations and not self.flow and not self.detector:
         if name == "relations":
            self.relations = False
         else:
            raise Exception("Error (/relations):" + name)

      else:
         raise Exception("Error: (/):" + name, self.relations, self.flow, self.detector)


def getPopulation(filename=POPULATION_FILENAME, which=0):
    tlh = PopulationHandler(which)
    saxparser = make_parser()
    saxparser.setContentHandler(tlh)
    datasource = open(filename,"r")
    saxparser.parse(datasource)
    return tlh.parameterDict

def getRoutes(filename=ROUTES_FILENAME):
    tlh = RoutesHandler()
    saxparser = make_parser()
    saxparser.setContentHandler(tlh)
    datasource = open(filename,"r")
    saxparser.parse(datasource)
    return tlh.sourceDict

def getRoutesZones(filename=ROUTES_FILENAME):
    tlh = RoutesZonesHandler()
    saxparser = make_parser()
    saxparser.setContentHandler(tlh)
    datasource = open(filename,"r")
    saxparser.parse(datasource)
    return tlh.sourceDict

def getTripList(tripInfoFile):
    tlh = TripHandler()
    saxparser = make_parser()
    saxparser.setContentHandler(tlh)
    datasource = open(tripInfoFile,"r")
    saxparser.parse(datasource)
    return tlh.tripList

def getSummaryList(summaryFile):
    tlh = SummaryHandler()
    saxparser = make_parser()
    saxparser.setContentHandler(tlh)
    datasource = open(summaryFile,"r")
    saxparser.parse(datasource)
    return tlh.summaryList

def getNetList(netFile):
   """Parses the network and returns the lane list with coords"""
   tlh = NetHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(netFile,"r")
   saxparser.parse(datasource)
   return tlh.laneList

def getPolyList(polyFile):
   """Parses the polygons and returns a list with coords"""
   tlh = PolyHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(polyFile,"r")
   saxparser.parse(datasource)
   return tlh.polyList

def getGeoDict(geoFile):
   tlh = GeoHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(geoFile,"r")
   saxparser.parse(datasource)
   return tlh.geoDict

def getVehicleDict(netstateFile):
   tlh = NetStateHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(netstateFile,"r")
   saxparser.parse(datasource)
   return tlh.vehicleDict

def getEmissions(emissionFile):
   tlh = EmissionHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(emissionFile,"r")
   saxparser.parse(datasource)
   return tlh.vehicleDict

def getZones(zoneFile):
   """Parses the zone file and returns the structure"""
   tlh = ZoneHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(zoneFile,"r")
   saxparser.parse(datasource)
   return tlh.zoneDict

def getSensors(sensorFile):
   """Parses the sensor file and returns the structure"""
   tlh = SensorHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(sensorFile,"r")
   saxparser.parse(datasource)
   return tlh.sensorDict

def getDetectors(detectorFile,start):
   """Parses the detector file and returns the structure"""
   tlh = DetectorHandler(start)
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(detectorFile,"r")
   saxparser.parse(datasource)
   return tlh.detectorDict

def getSolution(solutionFile):
   """Parses the solution file and returns the structure"""
   tlh = SolutionHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(solutionFile,"r")
   saxparser.parse(datasource)
   return tlh.solutionDict

def getFlows(flowFile):
   """Parses the flow file and returns the structure"""
   tlh = FlowHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(flowFile,"r")
   saxparser.parse(datasource)
   return tlh.flowDict

def getRelations(relationFile):
   """Parses the relation file and returns the structure"""
   tlh = RelationHandler()
   saxparser = make_parser()
   saxparser.setContentHandler(tlh)
   datasource = open(relationFile,"r")
   saxparser.parse(datasource)
   return tlh.relationDict

#def backupParameter():
    #now = datetime.today()
    #filename = BACKUP_FILEBASE+now.strftime("%Y%m%d%I%M%S")+".xml"
    #shutil.move(PARAM_FILENAME, filename)
    #print "Backup saved in", filename

def writeIndividual(f,D,T):
   keys = D.keys()
   keys.sort()
   for d in keys:
      f.write(T+'<input id="'+d+'" value="'+str(D[d])+'"/>\n')

def saveSensorList(sensorList):
   f = open(PARAM_FILENAME, 'w')
   f.write('<?xml version="1.0"?>\n')
   f.write('<parameters>\n')
   writeIndividual(f,sensorList,T1)
   f.write('</parameters>\n')
   f.close()

def savePopulation(population,fileName):
   f = open(fileName, 'w')
   f.write('<?xml version="1.0"?>\n')
   f.write('<population>\n')
   for i in range(len(population)):
      f.write(T1+'<individual id="'+str(i)+'" fitness="'+str(population[i]['fitness'])+'">\n')
      writeIndividual(f,population[i]['data'],T2)
      f.write(T1+'</individual>\n')
   f.write('</population>\n')
   f.close()

def saveIndividual(data,fileName):
   f = open(fileName, 'w')
   f.write('<?xml version="1.0"?>\n')
   f.write('<population>\n')
   f.write(T1+'<individual id="0" fitness="'+str(sys.maxint)+'">\n')
   writeIndividual(f,data,T2)
   f.write(T1+'</individual>\n')
   f.write('</population>\n')
   f.close()


def saveZones(zones,fileName):
   f = open(fileName, 'w')
   f.write('<?xml version="1.0"?>\n')
   f.write('<zones>\n')

#{z1.1
#     {'sensors': set(['35903052#2']),
#      'streets': set(['27618005', '35903052#1', '35903052#3'])}}

   zk = zones.keys()
   zk.sort()
   for z in zk:
      f.write(T1+'<zone id="'+z+'">\n')
      f.write(T2+'<sensors>\n')
      sensors = list(zones[z]['sensors'])
      sensors.sort()
      for s in sensors:
         f.write(T3+'<sensor id="'+s+'"/>\n')
      f.write(T2+'</sensors>\n')
      f.write(T2+'<streets>\n')
      streets = list(zones[z]['streets'])
      streets.sort()
      for s in streets:
         f.write(T3+'<street id="'+s+'"/>\n')
      f.write(T2+'</streets>\n')
      f.write(T1+'</zone>\n')
   f.write('</zones>\n')
   f.close()

def saveSensors(sensors,fileName):
   f = open(fileName, 'w')
   f.write('<?xml version="1.0"?>\n')
   f.write('<sensors>\n')

   zk = sensors.keys()
   zk.sort()
   for z in zk:
      f.write(T1+'<sensor id="'+z+'">\n')
      f.write(T2+'<streets>\n')
      streets = list(sensors[z])
      streets.sort()
      for s in streets:
         f.write(T3+'<street id="'+s+'"/>\n')
      f.write(T2+'</streets>\n')
      f.write(T1+'</sensor>\n')
   f.write('</sensors>\n')
   f.close()

