#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

T1 = "   "
T2 = T1+T1
T3 = T2+T1
T4 = T3+T1
T5 = T4+T1

SEP = ";"
LINE =    "--------------------------------------------------------------------------------"
DBLLINE = "================================================================================"
HEADER = "Seed"+SEP+"Fitness"+SEP+"Loaded"+SEP+"Emitted"+SEP+"Ended"+SEP+"DepartDelay"+SEP+"WaitSteps"+SEP+"Duration"+SEP+"Co"+SEP+"Co2"+SEP+"Hc"+SEP+"Pm"+SEP+"No"+SEP+"Fuel"+SEP+"LastArrival\n"
#

FITNESS_FORMAT = '%0.3f'
TIME_FORMAT = '%0.3f'

TRIPINFO_FILENAME = "tripinfo.out.xml"
RUN_FILENAME = "run.csv"
INFO_FILENAME = "info.txt"
STATUS_FILENAME = "0000000000.xml"
SUMMARY_FILENAME = "summary.out.xml"
NETSTATE_FILENAME = "netstate.out.xml"
EMISSION_FILENAME = "emissions.out.xml"
#PARAM_FILENAME = "parameters-test.xml"
ROUTES_FILENAME = "routes.xml"
POPULATION_FILENAME = "population.xml"
BACKUP_FILEBASE = "backup/parameters"

# html
HTML_DIR = "../html/"
PATTERNFILE = HTML_DIR + "html.pattern"
