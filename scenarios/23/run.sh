#!/bin/bash
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php

echo "2014 working days"
../bin/sumo -c 2014.wd.sumocfg --xml-validation.net never -X never
python ../bin/vehicles.py 1200 2014.wd.real.xml out.xml

echo "2014 Saturdays"
../bin/sumo -c 2014.sat.sumocfg --xml-validation.net never -X never
python ../bin/vehicles.py 1200 2014.sat.real.xml out.xml

echo "2014 Sundays"
../bin/sumo -c 2014.sun.sumocfg --xml-validation.net never -X never
python ../bin/vehicles.py 1200 2014.sun.real.xml out.xml

echo "2015 working days"
../bin/sumo -c 2015.wd.sumocfg --xml-validation.net never -X never
python ../bin/vehicles.py 1200 2015.wd.real.xml out.xml

echo "2015 Saturdays"
../bin/sumo -c 2015.sat.sumocfg --xml-validation.net never -X never
python ../bin/vehicles.py 1200 2015.sat.real.xml out.xml

echo "2015 Sundays"
../bin/sumo -c 2015.sun.sumocfg --xml-validation.net never -X never
python ../bin/vehicles.py 1200 2015.sun.real.xml out.xml
