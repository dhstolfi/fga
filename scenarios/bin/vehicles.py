#!/usr/bin/env python
# -*- coding: utf-8#
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#
import sys
from xml.dom import minidom

if len(sys.argv) < 4:
  print "Usage %s begin real sim" % (sys.argv[0])
  exit(1)

begin =int(sys.argv[1])
realFile = sys.argv[2]
simFile = sys.argv[3]

real = dict()
xmldoc = minidom.parse(realFile)
itemlist = xmldoc.getElementsByTagName('detector')
for s in itemlist:
  _id = s.attributes['id'].value.encode('ascii')
  _value = int(s.attributes['value'].value.encode('ascii'))
  real[_id] = _value

sim = dict()
xmldoc = minidom.parse(simFile)
itemlist = xmldoc.getElementsByTagName('interval')
for s in itemlist:
  _id = s.attributes['id'].value.encode('ascii')[:-2]
  _vh = int(s.attributes['nVehContrib'].value.encode('ascii'))
  _begin = float(s.attributes['begin'].value.encode('ascii'))
  if _begin >= begin:
    if _id in sim:
      _vh += sim[_id]
      sim.update(dict([(_id, _vh)]))
    else:
      sim[_id] = _vh

max_perc = 0.0
print "Sensor Real   Sim.   diff.  %"
print "------ ------ ------ ------ ------"
for s in real:
  diff = sim[s] - real[s]
  perc = float(sim[s]) / real[s] - 1.0
  max_perc = max(max_perc, abs(perc))
  print "%6s" % s, "%6d" % real[s], "%6d" % sim[s], "%6d" % diff, "%6.2f" % perc
print "                            ------"
print "                       Max: %6.2f" % max_perc

