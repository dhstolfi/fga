#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

import os, time, random, sys, argparse, csv, math
#from datetime import datetime
sys.path.append('../lib')
import utils, params, cons, myxml

# globals
POP = [] # population
NEW = [] # offspring
#SELECTED = [] # individuals selected
SOLUTION = {}
RELATIONS = {}
SENSORS = {}
OPTIONS = None
PD = 0

def get_options():
   parser = argparse.ArgumentParser(description='''Evolutionary Algorithm''')
   parser.add_argument("-c1", "--config1", help="the SUMO config file 1",
                     action="store", dest="configFile1", required=True)
   parser.add_argument("-c2", "--config2", help="the SUMO config file 2",
                     action="store", dest="configFile2", required=False)
   parser.add_argument("-t", "--template", help="the route template file",
                     action="store", dest="templateFile", required=True)
   parser.add_argument("-l", "--solution", help="the solution file",
                     action="store", dest="solutionFile", required=True)
   parser.add_argument("-o1", "--output1", help="the detector output file 1",
                     action="store", dest="detectorFile1", required=True)
   parser.add_argument("-o2", "--output2", help="the detector output file 2",
                     action="store", dest="detectorFile2", required=False)
   parser.add_argument("-r", "--relation", help="the relation file",
                     action="store", dest="relationFile", required=True)
   parser.add_argument("-a", "--start", help="the starting point",
                     action="store", dest="start", required=True, type=int)
   parser.add_argument("-b", "--total-time", help="the total simulation time",
                     action="store", dest="time", required=True, type=int)
   parser.add_argument("-s", "--random-seed", help="the random seed list (comma separated)",
                     action="store", dest="seeds", required=True)
   parser.add_argument("-v", "--verbose", help="increase output verbosity",
                     action="store_true", default=False, dest="verbose")
   parser.add_argument("-g", "--gui", help="run with GUI",
                     action="store_true", default=False, dest="gui")
   options = parser.parse_args()
   return options

def minFitness(indexes):
   """Returns the individual with the minimum fitness"""
   minFitness = sys.float_info.max
   val = -1
   for i in indexes:
#      print "pop:",POP[i]['fitness']
      if POP[i]['fitness']<minFitness:
         val = i
         minFitness = POP[i]['fitness']
#      print "min:",minFitness
#   print "min from:",indexes,"is",val
   return val


def generateIndividual():
   """Generates an individual"""

   IDS = utils.getFlowIds(OPTIONS.templateFile)

   T = {}
   # randomize
   #maximum = random.randint(params.MIN, params.MAX*params.INITIAL_VALUE)
   #maximum = params.MAX*params.INITIAL
   for i in IDS:
      value = 1
      if random.random() < params.INITIAL_FILL:
         maximum = round(params.MAX*params.INITIAL_VALUE)
         value = random.randint(params.MIN, maximum)
      T.update(dict([("%03d" % int(i), value)]))

   I = {'fitness':0, 'data':T, 'diff':{}}
   return I

def generatePopulation():
   """Generates the population"""

   global POP

   print "Generating initial populations"

   TEMP = []
   for i in range(params.POPULATION_SIZE * 10):
      TEMP.append(generateIndividual())
   DIS = []
   for t1 in TEMP:
      dist = 0
      for t2 in TEMP:
         dist += utils.distanceIndividuals(t1,t2)
      DIS.append(dist)

   MAX = utils.maxElements(DIS,params.POPULATION_SIZE)
   for i in range(0,len(MAX),params.OFFSPRINGS):

      I = []
      for j in range(params.OFFSPRINGS):
         index = i+j
         if utils.VERBOSE: print "Individual",index

         I.append(TEMP[MAX[index]])

      if utils.VERBOSE: print "Calculating fitness for individual(s)"

      fitnessList, diffList = fitnessValue(I)

      for j in range(len(fitnessList)):
         I[j]['fitness'] = fitnessList[j]
         I[j]['diff'] = diffList[j]
         POP.append(I[j])
   TEMP = []
   if utils.VERBOSE: print cons.LINE


def generateValidIndividual():
  """Generates a valid individual"""

  global RELATIONS


  TLIST = RELATIONS.keys()
  random.shuffle(TLIST)

  T = {}
  for t in TLIST:
    #value = random.randint(params.MIN, params.MAX / (len(RELATIONS[t]) * 5))
    #T.update(dict([("%03d" % int(t), value)]))
    value = params.MIN;
    if random.random() < params.INITIAL_FILL:
      maximum = max( round(params.MAX*params.INITIAL_VALUE), params.MIN )
      value = random.randint(params.MIN, maximum)
    T.update(dict([("%03d" % int(t), value)]))

  I = {'fitness':0, 'data':T, 'diff':{}}
  fitnessList, diffList = fitnessValue([I], False)
  I = {'fitness':fitnessList[0], 'data':T, 'diff':diffList[0]}


  detector = ""
  discard = True
  while discard:
    discard = False
    pos = 0
    D = diffList[0]
    for d in D:
       diff = float(D[d]) / SOLUTION[d]
       if diff > params.MAXDEV:   # if exeeds MAXDEV -> DISCARD
          discard = True
          detector = d
          if utils.VERBOSE:
            print "*** DISCARD",diff,">",params.MAXDEV
          break
       pos += 1
    if discard:
      for t in T:
        if detector in RELATIONS[t].keys() and T[t] > 1:
          #print T[t]
          T.update(dict([("%03d" % int(t), 1)]))
          #print T[t]
          break
      I = {'fitness':0, 'data':T, 'diff':{}}
      fitnessList, diffList = fitnessValue([I], False)
      I = {'fitness':fitnessList[0], 'data':T, 'diff':diffList[0]}

  return I

def generateValidPopulation():
   """Generates a valid population"""

   global POP

   print "Generating initial valid individuals"

   for i in range(params.POPULATION_SIZE):
      if utils.VERBOSE: print "Generating individual", i
      POP.append(generateValidIndividual())

   if utils.VERBOSE: print cons.LINE


def fitnessValue(I,parallel=True):

   global OPTIONS
   global SOLUTION

#   return [random.random(),random.random()],[{},{}]
#   parallel = False

   if parallel:
#   if OPTIONS.configFile2 != None and OPTIONS.detectorFile2 != None:
      return utils.fitness2(I[0], I[1], OPTIONS.configFile1, OPTIONS.configFile2, OPTIONS.templateFile, OPTIONS.detectorFile1, OPTIONS.detectorFile2, OPTIONS.start, OPTIONS.time, SOLUTION, OPTIONS.seeds, gui=False)
   else:
      data_f = []
      data_d = []
      for i in I:
         f, d = utils.fitness(i, OPTIONS.configFile1, OPTIONS.templateFile, OPTIONS.detectorFile1, OPTIONS.start, OPTIONS.time, SOLUTION, OPTIONS.seeds, gui=False)
         data_f.append(f)
         data_d.append(d)
      return [data_f, data_d]

def selection():
   """Selection Operator"""
   global POP
   global NEW
#   global SELECTED
   if len(POP) < 2*params.OFFSPRINGS:
      raise Exception("Population size < twice the number of offsprings ("+str(len(POP))+" < 2x"+str(params.OFFSPRINGS)+")")
   SELECTED = []
   SEL = []
   NEW = []
   while len(SEL)<2*params.OFFSPRINGS:
      rnd = random.randint(0, len(POP)-1)
      if not rnd in SEL:
         SEL.append(rnd)
   if utils.VERBOSE: print "Selecting individuals for tournament:",SEL

#   print "Fitnesses:",
#   for i in SEL:
#      print POP[i]['fitness'],
#   print

   for i in range(0,len(SEL),2):
      mini = minFitness(SEL[i:i+2])
      dice = random.random()
      if dice < params.TOURNAMENT:
         index = mini
      else:
         if mini == SEL[i]:
            index = SEL[i+1]
         else:
            index = SEL[i]
      SELECTED.append(index)
   for i in SELECTED:
      NEW.append(utils.copyIndividual(POP[i]))
   if utils.VERBOSE: print "Selecting individuals:",SELECTED

#   print "Fitnesses:",
#   for i in SELECTED:
#      print POP[i]['fitness'],
#   print
#   for N in NEW:
#      print N['fitness'],
#   print


def recombination():
   """Recombination Operator"""

   def crossover(I1,I2):
      """Uniform crossover"""
      if utils.VERBOSE:
         print "Crossing individuals"

      offSpring1 = utils.copyIndividual(I1)
      offSpring2 = utils.copyIndividual(I2)

      keys = offSpring1['data'].keys()
      for k in keys:
         if random.random() < 0.5:
            temp = offSpring1['data'][k]
            offSpring1['data'][k] = offSpring2['data'][k]
            offSpring2['data'][k] = temp

      offSpring1['fitness'] = sys.maxint
      offSpring2['fitness'] = sys.maxint

      return [offSpring1, offSpring2]

   global NEW

   TMP = []
   for i in range(0,len(NEW),2):
      if random.random()<params.CROSS_PROB:
         if utils.VERBOSE: print "Recombination of individuals:",i,"and",i+1
         TMP = TMP + crossover(NEW[i],NEW[i+1])
      else:
         TMP.append(NEW[i])
         TMP.append(NEW[i+1])

   NEW = []
   for i in range(len(TMP)):
     NEW.append(TMP[i])
   TMP = []

# # # # # # Mutating # # # # # #
def mutation(f):
   """Mutation"""

   # I
   # {'diff': {'10': -593, '13': -190, '15': -432, '14': -402, '17': -705, '16': -379, ... },
   #  'data': {'023': 10, '070': 10, '071': 43, '022': 10, '010': 53, '011': 10, ... },
   #  'fitness': 315.250845721336}

   # RELATIONS
   # {'005': {'18': 854, '5': 860},
   #  '070': {'15': 154, '7': 149},
   #  '071': {'15': 104},
   #  '004': {'5': 373}, ... }

   # SENSORS
   # {'10': ['038', '042', '044', '043', '040', '039'],
   #  '13': ['035', '052', '061', '023'],
   #  '15': ['070', '071', '038', '013', '036', '063', '065', '064', '003', '002', '001', '026', '028', '040', '012', '039'], ... }

   global SENSORS

   def mutate2(I):
     """Sensor orientated.
        Selects one sensor and changes one flow feeding that sensor.
        D depends on the number of flows and the difference in the measure."""

     # Selects one sensor
     sensors = SENSORS.keys()
     dice = random.randint(0,len(sensors)-1)
     sensor = sensors[dice]

     # Selects one flow feeding the sensor
     flows = SENSORS[sensor]
     dice = random.randint(0,len(flows)-1)
     flow = flows[dice]

     # Change the number of vehicles
     vehicles = I['data'][flow]
     diff = I['diff'][sensor]

     D = int(float(diff) / (-2.0 * len(flows)))
     if utils.VERBOSE: print "Vh:",vehicles,"D:",D,
     vehicles += D
     vehicles = min(params.MAX, max(params.MIN, vehicles))
     if utils.VERBOSE: print "Vh:",vehicles

     I['data'][flow] = vehicles
     return I

   def mutate1(I,D):
     """Flow orientated.
        Selects and changes flows according to PD.
        D depends on the best fitness of the population and the sign of the difference."""

     for flow in I['data']: # for each flow
       if random.random() <= PD:
         vehicles = I['data'][flow]
         sensors = RELATIONS[flow].keys()
         if sensors!=[]:
           dice = random.randint(0,len(sensors)-1) # pick one inductor
           sensor = sensors[dice]
           if I['diff'][sensor] < 0:    # if it's under the desired value
             vehicles += D            # increment
           else:                     # else
             vehicles -= D            # decrement
         else:
           print "WARNING: FLOW NOT DEPENDENT" # the new generator should avoid it to happen
           dice = random.random()
           if dice < 0.5:
             vehicles -= D
           else:
             vehicles += D
         vehicles = min(params.MAX, max(params.MIN, vehicles))
         I['data'][flow] = vehicles
     return I

   def mutate0(I,D):
     """Random.
        Selects and changes flows according to PD.
        D depends on the best fitness of the population and randomly adds of substracts vehicles."""

     for flow in I['data']: # for each flow
       if random.random() <= PD:
         vehicles = I['data'][flow]
         dice = random.random()
         if dice < 0.5:
           vehicles -= D
         else:
           vehicles += D
         vehicles = min(params.MAX, max(params.MIN, vehicles))
         I['data'][flow] = vehicles
     return I


   global NEW

   #D = int(max(params.ALPHA, f/params.BETA))  # 1500
   #D = int(round(20.0 * math.exp(f/3.0))) # 20.0
   D = int(round(params.ALPHA - math.sqrt(params.BETA) + math.sqrt( params.BETA * f))) # 20.0
   #D = int( max(round(f / 5.0), params.MIN) )
   if utils.VERBOSE: print "D:",D

   TMP = []
   for i in range(len(NEW)):
     # TODO: PM
     #TMP.append(mutate2(utils.copyIndividual(NEW[i])))
     TMP.append(mutate0(utils.copyIndividual(NEW[i]), D))

   NEW = []

#   4 evaluaciones por iteración
#   fitnessList,diffList = fitnessValue(TMP)
#   for i in range(len(TMP)):
#     TMP[i]['fitness'] = fitnessList[i]
#     TMP[i]['diff'] = diffList[i]
#     NEW.append(TMP[i])

#   2 evaluaciones por iteración
   for i in range(len(TMP)):
     NEW.append(TMP[i])
#

   TMP = []

def replacement():
   """Replacement Operator"""

   global POP
   global NEW
   global SOLUTION

   # evaluation
   for i in range(0,len(NEW),2):
      fitnessList,diffList = fitnessValue([NEW[i],NEW[i+1]])
      NEW[i]['fitness'] = fitnessList[i]
      NEW[i]['diff'] = diffList[i]
      NEW[i+1]['fitness'] = fitnessList[i+1]
      NEW[i+1]['diff'] = diffList[i+1]

   for I in NEW:
      discard = False
      D = I['diff']
      for d in D:
         diff = float(D[d]) / SOLUTION[d]
         if diff > params.MAXDEV:   # if exeeds MAXDEV -> DISCARD
            discard = True
            if utils.VERBOSE:
              print "*** DISCARD",diff,">",params.MAXDEV
            break
      if discard:
         #TODO: Repair
         I['fitness'] = sys.float_info.max

   if utils.VERBOSE:
     print "Replacing with elitism w/o repetitions"
#     print "Population before:"
#     for P in POP:
#       print P['fitness']
#     print
#     print "Population NEW:"


   #### Elitism w/o repetitions
   TMP = []
   for N in NEW:

#      if utils.VERBOSE:
#        print N['fitness']

      codN = utils.encodeData(N['data'])
      #print "testing", codN
      found = False
      for P in POP:
          found = utils.encodeData(P['data']) == codN
          if found:
              #print "found"
              break
      if not found:
         #print "not found"
         TMP.append(N)

   for I in POP:
      TMP.append(I)

   if utils.VERBOSE:
        print

   POP = []
   for i in range(params.POPULATION_SIZE):
      minFitness = sys.float_info.max
      M = {}
      for N in TMP:
         if N['fitness']<minFitness:
            M = N
            minFitness = N['fitness']
      POP.append(M)
      TMP.remove(M)

#   if utils.VERBOSE:
#     print "Population after:"
#     for P in POP:
#       print P['fitness']
#     print


   NEW = []
   TMP = []

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def main(options):

   global PD
   global NEW
   global POP
   global OPTIONS
   global SOLUTION
   global RELATIONS
   global SENSORS

   OPTIONS = options

   utils.VERBOSE = options.verbose
   step = 0
   minIndividual = 0
   avgFitness = sys.maxint


   def getStartingInfo():
      info = cons.DBLLINE
      info += "\nDirectory: "+RUN_DIR
      info += "\nBegin time: "+time.asctime( time.localtime(beginTime) )
      info += "\nRepetitions: "+str(params.REPETITIONS)
      info += "\nSimulation start: "+str(OPTIONS.start)
      info += "\nSimulation length: "+str(OPTIONS.time)
      info += "\nPopulation size: "+str(params.POPULATION_SIZE)
      info += "\nCrossover probability: "+str(params.CROSS_PROB)
      info += "\nTournament probability: "+str(params.TOURNAMENT)
      info += "\nConvergence: "+str(params.CONVERGENCE)
      info += "\nThreshold: "+str(params.THRESHOLD)
#      info += "\nMutation rate: "+str(params.MUTATION_RATE)+" ("+str(params.MIN)+"-"+str(params.MAX)+")"
      info += "\nAlpha: "+str(params.ALPHA)
      info += "\nBeta: "+str(params.BETA)
      info += "\nInitial Value: "+str(params.INITIAL_VALUE)
      info += "\nInitial Fill: "+str(params.INITIAL_FILL)
      info += "\nPD1: "+str(params.PD1)
      info += "\nPD2: "+str(params.PD2)
      info += "\nScenarios: "+options.seeds
      info += "\n"+cons.DBLLINE+"\n"
      return info

   def getEndingInfo():
      info = cons.DBLLINE
      info += "\nEnd time: "+time.asctime( time.localtime(time.time()) )
      info += "\nElapsed: "+str(time.time()-beginTime)
      info += "\nSteps: "+str(step)
      info += "\nHashTable Size: "+str(len(utils.TABLE))
      info += "\nBest fitness: "+str(minFitness)
      info += "\nBest individual: "+str(minIndividual)
      info += "\nAverage fitness: "+str(avgFitness)
      info += "\n"+cons.DBLLINE+"\n"
      return info

   ####
   # random.seed(0)
   ####

   PD = params.PD1

   # Begin the main section
   print cons.DBLLINE
   # directory for this run
   RUN_DIR = "run/"+time.strftime("%Y%m%d%H%M%S")

   beginTime = time.time()

   if not os.path.exists("run"):
      os.mkdir("run");
   os.mkdir(RUN_DIR) # if can't exists
   if utils.VERBOSE: print RUN_DIR+" created"

   fileName = RUN_DIR+"/"+cons.RUN_FILENAME

   logFile = open(fileName, 'wb', buffering=0)
   logWriter = csv.writer(logFile, delimiter=";", quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
   if utils.VERBOSE: print fileName+" created"

   print getStartingInfo()

   fileName = RUN_DIR+"/"+cons.INFO_FILENAME
   infoFile = open(fileName, 'w')
   infoFile.write(getStartingInfo())
   infoFile.close()

   SOLUTION = myxml.getSolution(options.solutionFile)
   RELATIONS = myxml.getRelations(options.relationFile);

   SENSORS = {}
   for flow in RELATIONS:
     sensors = RELATIONS[flow]
     for sensor in sensors.keys():
       flows = list()
       if sensor in SENSORS:
         flows = SENSORS[sensor]
       flows.append(flow)
       SENSORS.update(dict([(sensor, flows)]))

#   G = 0
#   for s in SOLUTION:
#     G += SOLUTION[s]
#   G = math.log(params.BETA) / float(len(SOLUTION))

   logWriter.writerow(["Status file","Step","Best fitness","Best individual","Average fitness","Same","Time"])

   prevFitness = sys.maxint

   # Generate new initial configuration
   #generateValidPopulation()
   generateValidPopulation()

#==============================================================================
#    utils.checkSolutions(POP)
#
   fileName = RUN_DIR+"/"+cons.STATUS_FILENAME
   myxml.savePopulation(POP,fileName)
   if utils.VERBOSE:
      print cons.LINE
      print "Status saved into "+ fileName

   minFitness = sys.float_info.max
   for p in POP:
      f = p['fitness']
      if f < minFitness:
         minFitness = f
   same = 0

   stage=1

   # Loop
   for step in range(1,params.REPETITIONS+1):

      start = time.time()

      #print cons.DBLLINE
      print "Begin step:",step
      if utils.VERBOSE: print cons.DBLLINE
      stepName = '%010d' % (step)

      selection()

      mutation(minFitness)

      recombination()

      replacement()

      minFitness = sys.float_info.max
      avgFitness = 0.0
      minIndividual = -1
      for i in range(len(POP)):
         f = POP[i]['fitness']
         avgFitness = avgFitness + f
         if f < minFitness:
            minFitness = f
            minIndividual = i
      avgFitness /= len(POP)

      same = same+1
      if prevFitness != minFitness:
         same = 0
      if minFitness == 0 or step == params.REPETITIONS or same >= params.CONVERGENCE:
         statusFileName = RUN_DIR+"/"+stepName+".xml"
         myxml.savePopulation(POP,statusFileName)
         if utils.VERBOSE: print "Status saved into "+ statusFileName

      prevFitness = minFitness
      end = time.time()
      diff = end-start

      # save logFile
      logWriter.writerow([RUN_DIR, step, minFitness, minIndividual, avgFitness, same, diff])

      if utils.VERBOSE:
         print cons.DBLLINE

      print "\nEnd step:",step, "of", params.REPETITIONS , "Generations:", same, "of", params.CONVERGENCE, "Time:",cons.TIME_FORMAT % diff
      print "Best fitness:", cons.FITNESS_FORMAT % (minFitness), "Best individual:", minIndividual, "Average Fitness:", cons.FITNESS_FORMAT % (avgFitness), "\n"
      #print cons.DBLLINE


      if minFitness == 0 or same >= params.CONVERGENCE:
         break

      if stage==1 and avgFitness <= params.THRESHOLD:
         if utils.VERBOSE:
            print "PD <- PD2"
         PD = params.PD2
         stage = 2

   logFile.close()

   print getEndingInfo()

   fileName = RUN_DIR+"/"+cons.INFO_FILENAME
   infoFile = open(fileName, 'a')
   infoFile.write(getEndingInfo())
   infoFile.close()
   print "END " + RUN_DIR

if __name__ == '__main__':
   main(get_options())

