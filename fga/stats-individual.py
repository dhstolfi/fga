#!/usr/bin/env python
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

import sys, argparse
sys.path.append('../lib')
from myxml import getPopulation, getSolution
#from parameters import *
import utils

parser = argparse.ArgumentParser(description='''Gets the stats from an individual''')
parser.add_argument("-c", "--config", help="the SUMO config file",
                    action="store", dest="configFile", required=True)
parser.add_argument("-s", "--random-seed", help="the random seed list (comma separated)",
                    action="store", dest="random", required=True)
parser.add_argument("-g", "--gui", help="run with GUI",
                    action="store_true", default=False, dest="gui")
parser.add_argument("-p", "--population", help="load population file",
                    action="store", default=None, dest="populationFile", required=True)
parser.add_argument("-t", "--template", help="the route template file",
                    action="store", dest="templateFile", required=True)
parser.add_argument("-l", "--solution", help="the solution file",
                    action="store", dest="solutionFile", required=True)
parser.add_argument("-o", "--output", help="the detector output file",
                    action="store", dest="detectorFile", required=True)
parser.add_argument("-a", "--start", help="the starting point",
                    action="store", dest="start", required=True, type=int)
parser.add_argument("-b", "--total-time", help="the total simulation time",
                    action="store", dest="time", required=True, type=int)
parser.add_argument("-i", "--individual", help="specify the individual",
                    action="store", default=0, dest="individual", type=int)
parser.add_argument("-f", "--stats-file", help="save stats in csv file",
                    action="store", default=None, dest="file")
parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true", default=False, dest="verbose")

options = parser.parse_args()

utils.VERBOSE = options.verbose

if options.populationFile != None:
   # Load from status
   print "Loading from status file:", options.populationFile,"("+str(options.individual)+")"
   SC = getPopulation(options.populationFile,options.individual)
   #print SC
   #SD = getPopulation("../results/CITIES/0.19/0000000909.xml",0)
   #print utils.fitness(SC['data'], options.configFile, options.routesFile, options.zoneFile,
   solDict = getSolution(options.solutionFile)
   print utils.fitness(SC, options.configFile, options.templateFile, options.detectorFile, options.start, options.time, solDict, options.random, gui=options.gui)
#==============================================================================
#    print utils.fitness([SC['data']], options.configFile, options.routesFile,
#                         options.port, seedList=options.random,
#                         statsFileName=options.file, gui=options.gui)
#==============================================================================

