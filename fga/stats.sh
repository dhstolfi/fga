#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

# usage: stats-individual.py [-h] -c CONFIGFILE -s RANDOM [-g] -p POPULATIONFILE
#                           -t TEMPLATEFILE -l SOLUTIONFILE -o DETECTORFILE -a
#                           START -b TIME [-i INDIVIDUAL] [-f FILE] [-v]

DIRS=(2014/wd/malaga.tt.ns.b09b 2014/sat/malaga.tt.ns.05 2014/sun/malaga.tt.ns.09
      2015/malaga.tt.ns.wd 2015/malaga.tt.ns.sat 2015/malaga.tt.ns.sun)
SOLS=(2014.wd.xml 2014.sat.xml 2014.sun.xml 2015.wd.xml 2015.sat.xml 2015.sun.xml)

for i in {0..5}
do
   DIR=${DIRS[$i]}
   SOL=${SOLS[$i]}
   echo "./stats-individual.py -c ../$DIR/malaga1.sumocfg -t ../$DIR/malaga.template.xml -l ../$DIR/solution.xml -o ../$DIR/out1.xml -p $SOL -a 1800 -b 5400 -s 0 -v"
   ./stats-individual.py -c ../$DIR/malaga1.sumocfg -t ../$DIR/malaga.template.xml -l ../$DIR/solution.xml -o ../$DIR/out1.xml -p $SOL -a 1800 -b 5400 -s 0 -v
done

