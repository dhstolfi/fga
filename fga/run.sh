#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

DIR=2014/wd/malaga.01
python2 fga3.py -c1 ../$DIR/malaga1.sumocfg -c2 ../$DIR/malaga2.sumocfg -t ../$DIR/malaga.template.xml -l ../$DIR/solution.xml -o1 ../$DIR/out1.xml -o2 ../$DIR/out2.xml -r ../$DIR/relations.xml -a 600 -b 4200 -s 0 -v
