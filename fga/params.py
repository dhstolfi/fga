#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Daniel H. Stolfi and Enrique Alba. Generating Realistic Urban Traffic Flows with Evolutionary Techniques. In: Engineering Applications of Artificial Intelligence, vol. 75, p. 36-47, 2018.
# doi> https://doi.org/10.1016/j.engappai.2018.07.009
# More info in https://en.danielstolfi.com/investigacion/problemas.php
#

REPETITIONS = 10000
POPULATION_SIZE = 10 #50
OFFSPRINGS = 2
CROSS_PROB = 0.9
PD1 = 5/57.0
PD2 = 1/57.0
TOURNAMENT = 1.0
MAXDEV = 0.10
INITIAL_VALUE = 0.10 #0.05 # tt.ns 0.10
INITIAL_FILL = 0.50 #0.25
ALPHA = 10.0
BETA = 19.587
MAX = 544 # max / 2
MIN = 10
THRESHOLD = 25.0
CONVERGENCE = 10000
